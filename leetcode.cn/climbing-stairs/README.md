# 爬楼梯

## 动态规划

### 先考虑最后一步，最后一步只有两种方法能到达顶端，爬一阶或者两阶，以此设dp[i]为爬到第i阶楼梯的方法，依次dp[i]=dp[i-1]+dp[i-2]；进而考虑这个题目的base case：dp[1]=1,dp[2]=2;