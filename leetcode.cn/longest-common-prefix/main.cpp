class Solution {
public:
    string longestCommonPrefix(vector<string>& strs) {
        int index = 0;
        for(int i = 1; i <strs.size(); i++){
            strs[i] = longest(strs[i - 1],strs[i]);
            if(strs[i] == ""){
                return "";
            }
        }
        return strs[strs.size() - 1];
    }
    string longest(string str1,string str2){
        int index = 0;
        int n = min(str1.size(),str2.size());
        for(int i = 0; i < n; i++){
            if(str1[i] == str2[i]){
                index++;
            }
            else{
                break;
            }
        }
        return str1.substr(0,index);
    }
};