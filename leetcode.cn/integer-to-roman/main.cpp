class Solution {
public:
    string intToRoman(int num) {
        int i = 0;
        int sum[] = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
        string str[] = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
        string t;
        while(num>0){
            while(num>=sum[i]){
                num -= sum[i];
                t += str[i];
            }
            i++;  
        }
        return t;
    }
};
