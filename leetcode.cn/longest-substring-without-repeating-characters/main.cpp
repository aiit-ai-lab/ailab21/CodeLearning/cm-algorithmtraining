class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        unordered_set<char>hash;
        int left = 0; 
        int ans = 0;
        for(int i = 0; i < s.size(); i++){          
            while(hash.find(s[i]) != hash.end()){
                hash.erase(s[left]);
                left++;
            }
            hash.insert(s[i]);
            ans =max(ans,i-left +1);
        }
        return ans;
    }
};